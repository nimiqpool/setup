#!/bin/bash
# Additional script to keep the miner always running!


#Get Settings

echo 'Using NimiqPool.com'
echo 'Using Mainnet'
echo 'Using mine.json'
nimiqScript="mine.json"

echo 'Enter Nimiqpool.com Username (Optional): '
read nimiqUser

#Generate Mining Runscript
touch $nimiqScript
>$nimiqScript
cat <<EOT >> $nimiqScript
{
  "apps" : [{
    "name"        : "nimiqpool-client",
    "script"      : "./client-core/clients/nodejs/index.js",
    "watch": false,
    "cwd": "./client-core/clients/nodejs",
    "args": [
EOT

if [ "$nimiqUser" != "" ]; then
        echo 'Enter the name of your machine for detailed stats on Nimiqpool.com'
        read nimiqMachine
        echo "\"REMOTE\", \"$nimiqUser\", \"$nimiqMachine\"" >> $nimiqScript

else
        echo 'Please enter the number of Miningthreads: '
        read nimiqThreads

        echo 'Enter Wallet Address (NOT SEED): '
        read nimiqAddress

        echo "\"LOCAL\", \"$nimiqAddress\", \"$nimiqThreads\"" >> $nimiqScript
fi
cat <<EOT >> $nimiqScript
],
    "env": {
      "UV_THREADPOOL_SIZE": "128"
    }
  }]
}
EOT


sudo npm install -g pm2
pm2 start mine.json
pm2 startup
pm2 save


