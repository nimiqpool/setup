#!/bin/bash
# Easy Setup Script for NodeJS Nimiq Miner


#Get Settings

echo 'Using NimiqPool.com'
echo 'Using Mainnet'
echo 'Using mine.sh'
nimiqScript="mine.sh"

echo 'Enter Nimiqpool.com Username (Optional): '
read nimiqUser

#Generate Mining Runscript
touch $nimiqScript
chmod +x $nimiqScript

echo "cd client-core && git pull && yarn " > $nimiqScript 

echo "cd clients/nodejs/" >> $nimiqScript 


if [ "$nimiqUser" != "" ]; then
        echo 'Enter the name of your machine for detailed stats on Nimiqpool.com'
        read nimiqMachine  
        echo "env UV_THREADPOOL_SIZE=128 node index.js REMOTE \"$nimiqUser\" \"$nimiqMachine\"" >> $nimiqScript

else    
        echo 'Please enter the number of Miningthreads: '
        read nimiqThreads

        echo 'Enter Wallet Address (NOT SEED): '
        read nimiqAddress

        echo "env UV_THREADPOOL_SIZE=$nimiqThreads node index.js LOCAL \"$nimiqAddress\" $nimiqThreads" >> $nimiqScript
fi


#Required Setup
sudo apt-get update
sudo apt-get -y upgrade

#Install requirements
sudo apt-get install -y curl git build-essential python

#Setup NodeJS
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo npm install -g yarn
yarn global add gulp

git clone https://gitlab.com/nimiqpool/client-core.git


