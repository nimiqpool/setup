# NimiqPool.com Setup



## Setup everything for Nimiq mining on Ubuntu/Debian:
```sh
bash <(wget -qO- https://gitlab.com/nimiqpool/setup/raw/master/Ubuntu_Debian.sh)
```
To start mining just type ``./mine.sh`` or ``sudo ./mine.sh``



## Additional script to keep the miner always running:
```sh
bash <(wget -qO- https://gitlab.com/nimiqpool/setup/raw/master/Add_AutoRestart.sh)
```
Make sure you have run the setup script before and that you are in the parent directory of client-core.
The miner will automatically start after the script is finished.
